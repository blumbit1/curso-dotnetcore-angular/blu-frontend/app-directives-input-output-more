import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgIfComponent } from './components/directives/ng-if/ng-if.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgForComponent } from './components/directives/ng-for/ng-for.component';
import { NgClassComponent } from './components/directives/ng-class/ng-class.component';
import { InputMaskComponent } from './components/directives/custom/input-mask/input-mask.component';
import { InputMaskDirective } from './custom-directives/input-mask.directive';
import { HeaderComponent } from './shared/header/header.component';
import { FormsModule } from '@angular/forms';
import { IchildComponent } from './components/input-output-view/input-demo/ichild/ichild.component';
import { IparentComponent } from './components/input-output-view/input-demo/iparent/iparent.component';
import { OchildComponent } from './components/input-output-view/output-demo/ochild/ochild.component';
import { OparentComponent } from './components/input-output-view/output-demo/oparent/oparent.component';
import { VchildComponent } from './components/input-output-view/view-child-demo/vchild/vchild.component';
import { VparentComponent } from './components/input-output-view/view-child-demo/vparent/vparent.component';

@NgModule({
  declarations: [
    AppComponent,
    NgIfComponent,
    NgForComponent,
    NgClassComponent,
    InputMaskComponent,
    InputMaskDirective,
    HeaderComponent,
    IchildComponent,
    IparentComponent,
    OchildComponent,
    OparentComponent,
    VchildComponent,
    VparentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
