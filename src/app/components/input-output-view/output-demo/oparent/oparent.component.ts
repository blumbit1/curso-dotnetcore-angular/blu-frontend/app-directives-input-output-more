import { Component } from '@angular/core';

@Component({
  selector: 'app-oparent',
  templateUrl: './oparent.component.html'
})
export class OparentComponent {

  exampleParent?: string;

  exampleMethodParent($event: any) {
    this.exampleParent = $event;
  }
}
