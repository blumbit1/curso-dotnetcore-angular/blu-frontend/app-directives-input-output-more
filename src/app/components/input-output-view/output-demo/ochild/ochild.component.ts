import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-ochild',
  templateUrl: './ochild.component.html'
})
export class OchildComponent {

  @Output() exampleOutput = new EventEmitter<string>();

  // Variable
  exampleChild: string = 'Hello from Blumbit, again!';

  // Metodo
  exampleMethodChild() {
    this.exampleOutput.emit(this.exampleChild);
  }
}
