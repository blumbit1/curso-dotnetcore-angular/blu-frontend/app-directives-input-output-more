import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { VchildComponent } from '../vchild/vchild.component';

@Component({
  selector: 'app-vparent',
  templateUrl: './vparent.component.html'
})
export class VparentComponent implements AfterViewInit{

  // Uso del @ViewChild para referirnos al elemento Hijo
  @ViewChild(VchildComponent) childReference!: any;

  exampleParent?: string;

  // Definir el método ngAfterViewInit
  ngAfterViewInit(): void {
      this.exampleParent = this.childReference.exampleChild;
  }
}
