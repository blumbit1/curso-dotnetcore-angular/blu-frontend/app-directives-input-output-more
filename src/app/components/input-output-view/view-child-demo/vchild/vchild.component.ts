import { Component } from '@angular/core';

@Component({
  selector: 'app-vchild',
  templateUrl: './vchild.component.html'
})
export class VchildComponent {

  exampleChild: string = 'Hello from Blumbit again, again...';
}
