import { Component } from '@angular/core';

@Component({
  selector: 'app-iparent',
  templateUrl: './iparent.component.html'
})
export class IparentComponent {

  parentExample: string = 'Hello from Blumbit';
}
