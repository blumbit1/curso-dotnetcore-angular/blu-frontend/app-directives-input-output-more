import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ichild',
  templateUrl: './ichild.component.html'
})
export class IchildComponent {

  @Input() childExample?: string;
}
