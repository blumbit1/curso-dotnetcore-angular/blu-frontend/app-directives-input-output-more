import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.scss']
})
export class NgClassComponent {

  //Aplicando de forma dinamica estilos desde una clase al componente
  cssVar: string = 'primary big';

  cssArray: string[] = ['secondary', 'big'];

  cssClass = {
    secondary: false,
    big: false,
  };
  flag: boolean = false;
}
