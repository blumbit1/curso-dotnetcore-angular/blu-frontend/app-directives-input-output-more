import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  template: `
              <div *ngIf="isLoggedIn">
                <p>Bienvenido {{ username }} a la familia Blumbit!</p>
              </div>
            `
})
export class NgIfComponent {

  // Variables
  isLoggedIn: boolean = true;
  username: string = 'Rodrigo Diaz de Carreras';
}
