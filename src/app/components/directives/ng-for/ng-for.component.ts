import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  template: `<label for="listado">Listado de itemes</label>
              <ul>
                <li *ngFor="let item of items">{{ item }}</li>
              </ul>
            `
})
export class NgForComponent {
  items = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6', 'Item 7', 'Item 8', 'Item 9', 'Item 10'];
}
