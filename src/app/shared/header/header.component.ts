import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="/assets/Logo-Blumbit.jpg" alt="Logo" width="50" height="55" class="d-inline-block align-text-top" />
          Blumbit Angular Directives
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" routerLink="/ng-if" routerLinkActive="active">Directiva ngIf</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/ng-for" routerLinkActive="active">Directiva ngFor</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/ng-class" routerLinkActive="active">Directiva ngClass</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/custom-directive" routerLinkActive="active">Directiva personalizada</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/input" routerLinkActive="active">@Input</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/output" routerLinkActive="active">@Output</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLink="/view-child" routerLinkActive="active">@Viewchild</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
})
export class HeaderComponent {}
