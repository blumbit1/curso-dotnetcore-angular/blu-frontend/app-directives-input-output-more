import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgIfComponent } from './components/directives/ng-if/ng-if.component';
import { NgForComponent } from './components/directives/ng-for/ng-for.component';
import { NgClassComponent } from './components/directives/ng-class/ng-class.component';
import { InputMaskComponent } from './components/directives/custom/input-mask/input-mask.component';
import { IparentComponent } from './components/input-output-view/input-demo/iparent/iparent.component';
import { OparentComponent } from './components/input-output-view/output-demo/oparent/oparent.component';
import { VparentComponent } from './components/input-output-view/view-child-demo/vparent/vparent.component';


const routes: Routes = [
  { path: '', redirectTo: 'ng-if', pathMatch: 'full'},
  { path: 'ng-if', component: NgIfComponent },
  { path: 'ng-for', component: NgForComponent },
  { path: 'ng-class', component: NgClassComponent },
  { path: 'custom-directive', component: InputMaskComponent },
  { path: 'input', component: IparentComponent },
  { path: 'output', component: OparentComponent },
  { path: 'view-child', component: VparentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
